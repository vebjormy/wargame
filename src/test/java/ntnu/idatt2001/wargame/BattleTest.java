package ntnu.idatt2001.wargame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class BattleTest {

    @Test
    @DisplayName("Tests if one side is empty after battle")
    void battleTest() {
        Battle battle = new Battle(ArmySelector.getInstance().getAllianceArmy(), ArmySelector.getInstance().getHordeArmy());
        battle.simulate();

        assert (!ArmySelector.getInstance().getPostBattleAllianceArmy().hasUnits() || !ArmySelector.getInstance().getPostBattleHordeArmy().hasUnits());
    }

}