package ntnu.idatt2001.wargame.units;

import ntnu.idatt2001.wargame.terrain.CurrentTerrain;
import ntnu.idatt2001.wargame.terrain.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RangedUnitTest {

    @Test
    @DisplayName("Testing the constructor")
    void rangedConstructorTest() {
        RangedUnit rangedUnit = new RangedUnit("Ranged", 40, 20, 12);

        assertEquals("Ranged", rangedUnit.getName());
        assertEquals(40, rangedUnit.getHealth());
        assertEquals(20, rangedUnit.getAttack());
        assertEquals(12, rangedUnit.getArmor());
    }

    @Test
    @DisplayName("Testing the simplified constructor")
    void rangedSimplifiedConstructorTest() {
        RangedUnit rangedUnit = new RangedUnit("Ranged", 30);

        assertEquals("Ranged", rangedUnit.getName());
        assertEquals(30, rangedUnit.getHealth());
        assertEquals(15, rangedUnit.getAttack());
        assertEquals(8, rangedUnit.getArmor());
    }

    @Test
    @DisplayName("Checking attackBonus and resistBonus values with health")
    void rangedAttackResistBonusTest() {
        RangedUnit rangedUnit = new RangedUnit("Ranged", 100);
        RangedUnit rangedAttacker = new RangedUnit("Attacker", 100);

        assertEquals(3, rangedUnit.getAttackBonus());
        assertEquals(6, rangedUnit.checkResistBonus());
        assertEquals(100, rangedUnit.getHealth());

        rangedAttacker.attack(rangedUnit);
        assertEquals(4, rangedUnit.checkResistBonus());
        assertEquals(99, rangedUnit.getHealth());

        rangedAttacker.attack(rangedUnit);
        assertEquals(2, rangedUnit.checkResistBonus());
        assertEquals(96, rangedUnit.getHealth());

        rangedAttacker.attack(rangedUnit);
        assertEquals(91, rangedUnit.getHealth());

        rangedAttacker.attack(rangedUnit);
        assertEquals(86, rangedUnit.getHealth());
    }

    @Test
    @DisplayName("Checking terrain attack bonus")
    void rangedTerrainAttackBonusTest() {
        RangedUnit ran1 = new RangedUnit("1", 50);
        RangedUnit ran2 = new RangedUnit("2", 50);
        RangedUnit ran3 = new RangedUnit("3", 50);
        RangedUnit ran4 = new RangedUnit("4", 50);


        ran1.attack(ran2);

        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.HILL);

        ran3.attack(ran4);

        assertEquals(49, ran2.getHealth());
        assertEquals(37, ran4.getHealth());
    }

}