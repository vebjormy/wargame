package ntnu.idatt2001.wargame.units;

import ntnu.idatt2001.wargame.Army;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GetUnitsTest {


    @Test
    @DisplayName("Checking if the method to get Infantry Units from an army works")
    public void checkInfantryGetter() {
        InfantryUnit footman = new InfantryUnit("Footman", 100);
        CavalryUnit knight = new CavalryUnit("Knight", 100);
        RangedUnit archer = new RangedUnit("Archer", 100);
        CommanderUnit mountainKing = new CommanderUnit("Mountain King", 180);

        Army army = new Army("Norge");

        for (int i = 0; i < 10;i++) {
            army.add(footman);
        }
        for (int i = 0; i < 5; i++) {
            army.add(knight);
        }
        for (int i = 0; i < 7; i++) {
            army.add(archer);
        }
        army.add(mountainKing);

        assertEquals(10, army.getInfantryUnits().size());
    }

    @Test
    @DisplayName("Checking if the method to get Ranged Units from an army works")
    public void checkRangedGetter() {
        InfantryUnit footman = new InfantryUnit("Footman", 100);
        CavalryUnit knight = new CavalryUnit("Knight", 100);
        RangedUnit archer = new RangedUnit("Archer", 100);
        CommanderUnit mountainKing = new CommanderUnit("Mountain King", 180);

        Army army = new Army("Norge");

        for (int i = 0; i < 10;i++) {
            army.add(footman);
        }
        for (int i = 0; i < 5; i++) {
            army.add(knight);
        }
        for (int i = 0; i < 7; i++) {
            army.add(archer);
        }
        army.add(mountainKing);

        assertEquals(7, army.getRangedUnits().size());
    }

    @Test
    @DisplayName("Checking if the method to get Cavalry Units from an army works")
    public void checkCavalryGetter() {
        InfantryUnit footman = new InfantryUnit("Footman", 100);
        CavalryUnit knight = new CavalryUnit("Knight", 100);
        RangedUnit archer = new RangedUnit("Archer", 100);
        CommanderUnit mountainKing = new CommanderUnit("Mountain King", 180);

        Army army = new Army("Norge");

        for (int i = 0; i < 10;i++) {
            army.add(footman);
        }
        for (int i = 0; i < 5; i++) {
            army.add(knight);
        }
        for (int i = 0; i < 7; i++) {
            army.add(archer);
        }
        army.add(mountainKing);

        assertEquals(5, army.getCavalryUnits().size());
    }

    @Test
    @DisplayName("Checking if the method to get Commander Units from an army works")
    public void checkCommanderGetter() {
        InfantryUnit footman = new InfantryUnit("Footman", 100);
        CavalryUnit knight = new CavalryUnit("Knight", 100);
        RangedUnit archer = new RangedUnit("Archer", 100);
        CommanderUnit mountainKing = new CommanderUnit("Mountain King", 180);

        Army army = new Army("Norge");

        for (int i = 0; i < 10;i++) {
            army.add(footman);
        }
        for (int i = 0; i < 5; i++) {
            army.add(knight);
        }
        for (int i = 0; i < 7; i++) {
            army.add(archer);
        }
        army.add(mountainKing);

        assertEquals(1, army.getCommanderUnits().size());
    }
}
