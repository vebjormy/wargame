package ntnu.idatt2001.wargame.units;

import ntnu.idatt2001.wargame.terrain.CurrentTerrain;
import ntnu.idatt2001.wargame.terrain.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CavalryUnitTest {

    @Test
    @DisplayName("Testing the constructor")
    void cavalryConstructorTest() {
        CavalryUnit cavalryUnit = new CavalryUnit("Cavalry", 50, 20, 12);

        assertEquals("Cavalry", cavalryUnit.getName());
        assertEquals(50, cavalryUnit.getHealth());
        assertEquals(20, cavalryUnit.getAttack());
        assertEquals(12, cavalryUnit.getArmor());
    }

    @Test
    @DisplayName("Testing the simplified constructor")
    void cavalrySimplifiedConstructorTest() {
        CavalryUnit cavalryUnit = new CavalryUnit("Cavalry", 50);

        assertEquals("Cavalry", cavalryUnit.getName());
        assertEquals(50, cavalryUnit.getHealth());
        assertEquals(20, cavalryUnit.getAttack());
        assertEquals(12, cavalryUnit.getArmor());
    }


    @Test
    @DisplayName("Checking attackBonus and resistBonus with health")
    void cavalryAttackResistBonusTest() {
        CavalryUnit cavalryUnit = new CavalryUnit("Cavalry", 100);
        CavalryUnit cavalryAttacker = new CavalryUnit("Attacker", 100);

        assertEquals(0, cavalryAttacker.getResistBonus());
        cavalryAttacker.attack(cavalryUnit);
        assertEquals(1, cavalryAttacker.getResistBonus());
        assertEquals(0, cavalryUnit.getResistBonus());
        assertEquals(86, cavalryUnit.getHealth());

        cavalryAttacker.attack(cavalryUnit);
        assertEquals(76, cavalryUnit.getHealth());

    }

    @Test
    @DisplayName("Checking negative health")
    void cavalryNegativeHealthTest() {
        try {
            CavalryUnit cavalryUnit = new CavalryUnit("Cavalry", -1);
            CavalryUnit cavalryAttacker = new CavalryUnit("Attacker", 100);

            assertEquals(0, cavalryAttacker.getResistBonus());
            cavalryAttacker.attack(cavalryUnit);
            assertEquals(1, cavalryAttacker.getResistBonus());
            assertEquals(0, cavalryUnit.getResistBonus());
            assertEquals(86, cavalryUnit.getHealth());

            cavalryAttacker.attack(cavalryUnit);
            assertEquals(76, cavalryUnit.getHealth());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    @Test
    @DisplayName("Checking terrain attack bonus")
    void cavalryTerrainAttackBonusTest() {
        CavalryUnit cav1 = new CavalryUnit("1", 50);
        CavalryUnit cav2 = new CavalryUnit("2", 50);
        CavalryUnit cav3 = new CavalryUnit("3", 50);
        CavalryUnit cav4 = new CavalryUnit("4", 50);


        cav1.attack(cav2);

        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.PLAINS);

        cav3.attack(cav4);

        assertEquals(36, cav2.getHealth());
        assertEquals(25, cav4.getHealth());
    }

}