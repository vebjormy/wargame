package ntnu.idatt2001.wargame.units;

import ntnu.idatt2001.wargame.terrain.CurrentTerrain;
import ntnu.idatt2001.wargame.terrain.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InfantryUnitTest {


    @Test
    @DisplayName("Testing the constructor")
    void infantryConstructorTest() {
        InfantryUnit infantryUnit = new InfantryUnit("Infantry", 40, 20, 12);

        assertEquals("Infantry", infantryUnit.getName());
        assertEquals(40, infantryUnit.getHealth());
        assertEquals(20, infantryUnit.getAttack());
        assertEquals(12, infantryUnit.getArmor());
    }

    @Test
    @DisplayName("Testing the simplified constructor")
    void infantrySimplifiedConstructorTest() {
        InfantryUnit infantryUnit = new InfantryUnit("Infantry", 30);

        assertEquals("Infantry", infantryUnit.getName());
        assertEquals(30, infantryUnit.getHealth());
        assertEquals(15, infantryUnit.getAttack());
        assertEquals(10, infantryUnit.getArmor());
    }

    @Test
    @DisplayName("Checking attackBonus and resistBonus values")
    void infantryAttackResistBonusTest() {
        InfantryUnit infantryUnit = new InfantryUnit("Infantry", 20);

        assertEquals(2, infantryUnit.getAttackBonus());
        assertEquals(1, infantryUnit.getResistBonus());
    }

    @Test
    @DisplayName("Checking terrain attack bonus")
    void infantryTerrainAttackBonusTest() {
        InfantryUnit inf1 = new InfantryUnit("1", 50);
        InfantryUnit inf2 = new InfantryUnit("2", 50);
        InfantryUnit inf3 = new InfantryUnit("3", 50);


        inf1.attack(inf2);

        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.HILL);

        inf1.attack(inf3);

        assertEquals(41, inf2.getHealth());
        assertEquals(44, inf3.getHealth());
    }

    @Test
    @DisplayName("Checking terrain defense bonus")
    void infantryTerrainDefenseBonusTest() {
        InfantryUnit inf1 = new InfantryUnit("1", 50);
        InfantryUnit inf2 = new InfantryUnit("2", 50);
        CavalryUnit cav1 = new CavalryUnit("Cav1", 50);
        CavalryUnit cav2 = new CavalryUnit("Cav2", 50);

        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.FOREST);

        cav1.attack(inf1);

        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.HILL);

        cav2.attack(inf2);

        assertEquals(37, inf1.getHealth());
        assertEquals(35, inf2.getHealth());

    }

}