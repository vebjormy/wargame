package ntnu.idatt2001.wargame.units;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommanderUnitTest {


    @Test
    @DisplayName("Testing the constructor")
    void commanderConstructorTest() {
        CommanderUnit commanderUnit = new CommanderUnit("Commander", 180, 30, 20);

        assertEquals("Commander", commanderUnit.getName());
        assertEquals(180, commanderUnit.getHealth());
        assertEquals(30, commanderUnit.getAttack());
        assertEquals(20, commanderUnit.getArmor());
    }

    @Test
    @DisplayName("Testing the simplified constructor")
    void commanderSimplifiedConstructorTest() {
        CommanderUnit commanderUnit = new CommanderUnit("Commander", 180);

        assertEquals("Commander", commanderUnit.getName());
        assertEquals(180, commanderUnit.getHealth());
        assertEquals(25, commanderUnit.getAttack());
        assertEquals(15, commanderUnit.getArmor());
    }


    @Test
    @DisplayName("Checking attackBonus and resistBonus with health")
    void commanderAttackResistBonusTest() {
        CommanderUnit commanderUnit = new CommanderUnit("Commander", 180);
        CommanderUnit commanderAttacker = new CommanderUnit("Attacker", 180);

        commanderAttacker.attack(commanderUnit);
        assertEquals(164, commanderUnit.getHealth());

    }
}