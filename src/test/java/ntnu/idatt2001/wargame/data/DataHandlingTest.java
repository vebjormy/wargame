package ntnu.idatt2001.wargame.data;

import ntnu.idatt2001.wargame.Army;
import ntnu.idatt2001.wargame.factory.UnitFactory;
import ntnu.idatt2001.wargame.factory.UnitType;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


import java.io.File;
import java.io.FileNotFoundException;

class DataHandlingTest {


    /**
     * Asserts that writeToFile writes to file
     * Used for further testing
     */

    @Test
    void testWriteToFile() {
        assert(DataHandling.writeToFile(new File("src/test/resources/testfile.csv"), "Test en\ntest to, test tre"));
    }


    /**
     * Asserts that readFromFile reads from given file
     */
    @Test
    void testReadFromFile() {
        boolean success;

        try {
            assertEquals("Test en", DataHandling.readFromFile(new File("src/test/resources/testfile.csv")).get(0));
            success = true;
        } catch (FileNotFoundException e) {
            success = false;
        }
        assert(success);
    }


    /**
     * Negative test
     * Asserts that trying to read a non-existing file triggers exception
     */
    @Test
    void testCantReadFromFile() {
        boolean success;

        try {
            assertEquals("Test en", DataHandling.readFromFile(new File("src/test/resources/wrongfile.csv")).get(0));
            success = true;
        } catch (FileNotFoundException | IndexOutOfBoundsException e) {
            success = false;
        }

        assert(!success);
    }

    /**
     * Tests if the army-file saved is the same as the army that is created
     */
    @Test
    void testSaveToFileFactory() {
        UnitFactory uf = new UnitFactory();
        Army army1 = new Army("TestArmy");
        army1.addAll(uf.getMultipleUnits(UnitType.INFANTRY, "Footman", 100, 10));
        army1.addAll(uf.getMultipleUnits(UnitType.RANGED, "Archer", 80, 7));
        army1.addAll(uf.getMultipleUnits(UnitType.CAVALRY, "Knight", 100, 5));
        army1.addAll(uf.getMultipleUnits(UnitType.COMMANDER, "Mountain King", 180, 1));

        DataStorage.saveArmyToFile(army1);

        assertEquals(DataStorage.generateArmyFromFile("src/main/resources/TestArmyArmy.csv").toString(), army1.toString());
    }

    /**
     * Negative test to read from non-existing file
     */
    @Test
    void testReadFromFileCreatedUsingSaveFile() {
        try {
            DataHandling.readFromFile(new File("src/main/resources/NonExistingArmy.csv"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}