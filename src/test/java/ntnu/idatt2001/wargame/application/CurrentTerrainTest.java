package ntnu.idatt2001.wargame.application;

import ntnu.idatt2001.wargame.terrain.CurrentTerrain;
import ntnu.idatt2001.wargame.terrain.Terrain;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class CurrentTerrainTest {

    /**
     * Asserts if changing terrain works
     */
    @Test
    public void testCurrentTerrain() {
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.FOREST);
        assert (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.FOREST);

        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.HILL);
        assert (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.HILL);

        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.PLAINS);
        assert (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.PLAINS);
    }
}