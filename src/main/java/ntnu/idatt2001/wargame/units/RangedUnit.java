package ntnu.idatt2001.wargame.units;

import ntnu.idatt2001.wargame.terrain.Terrain;
import ntnu.idatt2001.wargame.terrain.CurrentTerrain;

public class RangedUnit extends Unit {

    private int resistBonusCounter = 0;


    /**
     * Constructor for the RangedUnit
     *
     * @param name   name of RangedUnit
     * @param health health of RangedUnit
     * @param attack attack of RangedUnit
     * @param armor  armor of RangedUnit
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }


    /**
     * Simplified constructor for the CavalryUnit with parameters name and health
     *
     * @param name   name of CavalryUnit
     * @param health health of CavalryUnit
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }


    /**
     * Method used for checking the current resistBonus
     *
     * @return the current value of resistBonus
     */
    public int checkResistBonus() {
        if (resistBonusCounter == 0) {
            return 6;
        } else if (resistBonusCounter == 1) {
            return 4;
        }
        return 2;
    }


    /**
     * Method for getting the attackBonus
     *
     * @return the attackBonus for RangedUnit
     */
    @Override
    public int getAttackBonus() {
        return 3;
    }


    /**
     * Modified method for getting the resistBonus for RangedUnit.
     * Returns different values based on the number of times it has been attacked
     * Uses the variable resistBonusCounter
     *
     * @return the resistBonus
     */
    @Override
    public int getResistBonus() {
        if (resistBonusCounter == 0) {
            resistBonusCounter++;
            return 6;
        } else if (resistBonusCounter == 1) {
            resistBonusCounter++;
            return 4;
        } else {
            return 2;
        }
    }


    /**
     * Method for changing attack value based on terrain
     *
     * @return -20% if in FOREST, +50% if in HILL
     */
    @Override
    public double terrainAttackModifier() {
        if (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.FOREST) {
            return 0.8;
        } else if (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.HILL) {
            return 1.5;
        }
        return 1;
    }


    /**
     * Method for changing defense value based on terrain
     *
     * @return normal value, this units defense is not impacted by terrain
     */
    @Override
    public double terrainDefenseModifier() {
        return 1;
    }
}
