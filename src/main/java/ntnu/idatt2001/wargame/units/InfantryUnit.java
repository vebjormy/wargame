package ntnu.idatt2001.wargame.units;

import ntnu.idatt2001.wargame.terrain.Terrain;
import ntnu.idatt2001.wargame.terrain.CurrentTerrain;

public class InfantryUnit extends Unit {

    /**
     * Constructor for the InfantryUnit
     *
     * @param name   name of Unit
     * @param health health of Unit
     * @param attack attack of Unit
     * @param armor  armor of Unit
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }


    /**
     * Simplified constructor for the InfantryUnit
     * Attack is set to 15
     * Armor is set to 10
     *
     * @param name   name of the Unit
     * @param health health of the Unit
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }


    /**
     * Method for getting the attackBonus
     * Always 2 since it only can attack when in melee
     *
     * @return the attackBonus for InfantryUnit
     */
    @Override
    public int getAttackBonus() {return 2;
    }


    /**
     * Method for getting the resistBonus
     *
     * @return the resistBonus for InfantryUnit
     */
    @Override
    public int getResistBonus() {
        return 1;
    }


    /**
     * Method for changing attack value based on terrain
     *
     * @return 30% increase in damage if in FOREST, and no increase if not
     */
    @Override
    public double terrainAttackModifier() {
        if (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.FOREST) {
            return 1.3;
        }
        return 1;
    }


    /**
     * Method for changing defense value based on terrain
     *
     * @return 20% increase in defense if in FOREST, and no increase if not
     */
    @Override
    public double terrainDefenseModifier() {
        if (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.FOREST) {
            return 1.2;
        }
        return 1;
    }
}
