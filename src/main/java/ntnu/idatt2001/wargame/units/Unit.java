package ntnu.idatt2001.wargame.units;

/**
 * The abstract class Unit, which is inherited by all other Unit-classes
 */
public abstract class Unit {

    private final String name;
    private int health;
    private final int attack;
    private final int armor;


    /**
     * Constructor for the abstract class Unit
     * Is inherited by InfantryUnit, RangedUnit, CavalryUnit and CommanderUnit
     *
     * @param name   name of Unit
     * @param health health of Unit
     * @param attack attack power of Unit
     * @param armor  armor of Unit
     * @throws IllegalArgumentException for values under 0
     */
    protected Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        if (name.isEmpty() || health <= 0 || attack < 0 || armor < 0) {
            throw new IllegalArgumentException("Values not permitted");
        }
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }


    /**
     * Method that allows a Unit to attack another Unit
     * Uses health, attack, armor, attackBonus, resistBonus and terrainModifier to calculate the new health of the attacked Unit
     *
     * @param opponent the Unit that is being attacked
     */
    public void attack(Unit opponent) {

        int newHealth = (int) (opponent.getHealth() - ((this.getAttack() + this.getAttackBonus()) * this.terrainAttackModifier())
                        + ((opponent.getArmor() + opponent.getResistBonus())) * opponent.terrainDefenseModifier());

        opponent.setHealth(Math.max(newHealth, 0));
    }


    /**
     * Method to get the name of a Unit
     *
     * @return the name of the Unit
     */
    public String getName() {
        return name;
    }


    /**
     * Method to get the health of a Unit
     *
     * @return the health of the Unit
     */
    public int getHealth() {
        return health;
    }


    /**
     * Method to get the attack of a Unit
     *
     * @return the attack of a Unit
     */
    public int getAttack() {
        return attack;
    }


    /**
     * Method to get the armor of a Unit
     *
     * @return the armor of a Unit
     */
    public int getArmor() {
        return armor;
    }


    /**
     * Method for calculating terrain attack-modifiers
     */
    public abstract double terrainAttackModifier();


    /**
     * Method for calculating terrain defense-modifiers
     */
    public abstract double terrainDefenseModifier();


    /**
     * Method to change the health of a Unit
     *
     * @param health set the new value of a Units health
     */
    public void setHealth(int health) {
        this.health = health;
    }


    /**
     * toString-method to be able to print Unit
     *
     * @return Unit with all values (name, health, attack, armor)
     */
    @Override
    public String toString() {
        return "{" + getClass().getSimpleName() +
                ", name = '" + name + '\'' +
                ", health = " + health +
                ", attack = " + attack +
                ", armor = " + armor +
                '}';
    }


    /**
     * Abstract method to get the attackBonus of a Unit
     *
     * @return the attackBonus of a Unit
     */
    public abstract int getAttackBonus();


    /**
     * Abstract method to get the resistBonus of a Unit
     *
     * @return the resistBonus of a Unit
     */
    public abstract int getResistBonus();


    /**
     * Method for getting the right format of a unit in the csv-file
     *
     * @return the information on a unit as a string
     */
    public String getCsvFormatUnit() {
        return getClass().getSimpleName() + "," + getName() + "," + getHealth();
    }
}
