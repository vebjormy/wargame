package ntnu.idatt2001.wargame.units;

import ntnu.idatt2001.wargame.terrain.Terrain;
import ntnu.idatt2001.wargame.terrain.CurrentTerrain;

public class CavalryUnit extends Unit {

    private int attackCounter = 1;
    boolean melee = false;


    /**
     * Constructor for the CavalryUnit
     *
     * @param name   name of CavalryUnit
     * @param health health of CavalryUnit
     * @param attack attack of CavalryUnit
     * @param armor  armor of CavalryUnit
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }


    /**
     * Simplified constructor for the CavalryUnit with parameters name and health
     *
     * @param name   name of CavalryUnit
     * @param health health of CavalryUnit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }


    /**
     * Modified method for getting the attackBonus for CavalryUnit.
     * Returns different values based on whether the unit is in melee or not
     *
     * @return the attackBonus
     */
    @Override
    public int getAttackBonus() {
        int charge = 4;

        if (attackCounter == 1) {
            attackCounter--;
            melee = true;
            return charge + 2;
        }
        return 2;
    }


    /**
     * Modified method for getting the resistBonus for CavalryUnit.
     * Returns different values based on whether the unit is in melee or not
     *
     * @return the resistBonus
     */
    @Override
    public int getResistBonus() {
        if (melee) {
            return 1;
        }
        return 0;
    }


    /**
     * Method for changing attack value based on terrain
     *
     * @return +40% when attacking in PLAINS
     */
    @Override
    public double terrainAttackModifier() {
        if (CurrentTerrain.getInstance().getCurrentTerrain() == Terrain.PLAINS) {
            return 1.4;
        }
        return 1;
    }


    /**
     * Method for changing defense value based on terrain
     *
     * @return normal value, this units defense is not impacted by terrain
     */
    @Override
    public double terrainDefenseModifier() {
        return 1;
    }
}
