package ntnu.idatt2001.wargame.units;


public class CommanderUnit extends CavalryUnit {

    /**
     * Constructor for the CommanderUnit
     *
     * @param name   name of CommanderUnit
     * @param health health of CommanderUnit
     * @param attack attack of CommanderUnit
     * @param armor  armor of CommanerUnit
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }


    /**
     * Simplified constructor for the CommanderUnit with the variables name and health
     *
     * @param name   name of CommanderUnit
     * @param health health of CommanderUnit
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }


    /**
     * Method for getting the attackBonus
     * Inheriting from CavalryUnit
     * Returns different values based on whether the unit is in melee or not
     *
     * @return the attackBonus
     */
    @Override
    public int getAttackBonus() {
        return super.getAttackBonus();
    }


    /**
     * Method for getting the resistBonus
     * Inheriting from the CavalryUnit
     * Returns different values based on whether the unit is in melee or not
     *
     * @return the resistBonus
     */
    @Override
    public int getResistBonus() {
        return super.getResistBonus();
    }


    /**
     * Mehod for changing attack value based on terrain
     *
     * @return normal value, this units attack is not impacted by terrain
     */
    @Override
    public double terrainAttackModifier() {
        return 1;
    }


    /**
     * Method for changing defense value based on terrain
     *
     * @return normal value, this units defense is not impacted by terrain
     */
    @Override
    public double terrainDefenseModifier() {
        return 1;
    }
}


