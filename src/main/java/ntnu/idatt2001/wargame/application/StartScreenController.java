package ntnu.idatt2001.wargame.application;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class StartScreenController {

    private Stage stage;
    private Scene scene;
    private Parent root;

    /**
     * Method for switching to Battle Selection screen
     *
     * @param event button pressed
     */
    @FXML
    public void switchToBattleSelection(ActionEvent event) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("battle-selection.fxml")));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method for exiting application
     *
     * @param actionEvent button pressed
     */
    @FXML
    private void quit(ActionEvent actionEvent) {
        Platform.exit();
    }
}
