package ntnu.idatt2001.wargame.application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ntnu.idatt2001.wargame.ArmySelector;
import ntnu.idatt2001.wargame.Battle;
import ntnu.idatt2001.wargame.data.DataStorage;
import ntnu.idatt2001.wargame.terrain.CurrentTerrain;
import ntnu.idatt2001.wargame.terrain.Terrain;

import java.io.IOException;
import java.util.Objects;

public class BattleSelectionController {

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Text terrainDescriptionText;

    @FXML
    private Text allianceInfantryAmount;

    @FXML
    private Text hordeInfantryAmount;

    @FXML
    private Text allianceRangedAmount;

    @FXML
    private Text hordeRangedAmount;

    @FXML
    private Text allianceCavalryAmount;

    @FXML
    private Text hordeCavalryAmount;

    @FXML
    private Text allianceCommanderName;

    @FXML
    private Text hordeCommanderName;


    /**
     * Method for switching to Start Screen
     * Also resets armies to small and terrain to forest
     *
     * @param event button pressed
     * @throws IOException
     */
    @FXML
    public void switchToStartScreen(ActionEvent event) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("start-screen.fxml")));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        ArmySelector.getInstance().resetArmies();
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.FOREST);
    }

    /**
     * Method for switching to Alliance Unit Info Screen
     *
     * @param event button pressed
     * @throws IOException
     */
    @FXML
    public void switchToAllianceUnitInfoScreen(ActionEvent event) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("alliance-unit-information.fxml")));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Method for switching to Horde Unit Info Screen
     *
     * @param event button pressed
     * @throws IOException
     */
    @FXML
    public void switchToHordeUnitInfoScreen(ActionEvent event) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("horde-unit-information.fxml")));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


    /**
     * Method for switching to End Screen
     * Simulates the battle
     * Also updates army-values to postBattle value and puts the values in the End Screen
     * Updates text to say who won
     *
     * @param event button pressed
     * @throws IOException
     */
    @FXML
    public void switchToEndScreen(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader((getClass().getClassLoader().getResource("end-screen.fxml")));
        root = loader.load();

        EndScreenController endScreenController = loader.getController();

        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        battle();
        ArmySelector.getInstance().setAllianceArmy(ArmySelector.getInstance().getPostBattleAllianceArmy());
        ArmySelector.getInstance().setHordeArmy(ArmySelector.getInstance().getPostBattleHordeArmy());
        endScreenController.setAliveArmySizeInGUIAlliance();
        endScreenController.setAliveArmySizeInGUIHorde();

        endScreenController.setWinnerText();
        }

    /**
     * Sets the terrain to Forest
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void terrainForest() {
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.FOREST);
        System.out.println("Terrain set: " + CurrentTerrain.getInstance().getCurrentTerrain());
        terrainDescriptionText.setText("|  Infantry: +30%attack +20%defense  |  Ranged: -20%attack  |");
    }

    /**
     * Sets the terrain to Forest
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void terrainHill() {
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.HILL);
        System.out.println("Terrain set: " + CurrentTerrain.getInstance().getCurrentTerrain());
        terrainDescriptionText.setText("|  Ranged: +50%attack  |");
    }

    /**
     * Sets the terrain to Forest
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void terrainPlains() {
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.PLAINS);
        System.out.println("Terrain set: " + CurrentTerrain.getInstance().getCurrentTerrain());
        terrainDescriptionText.setText("|  Cavalry +40%attack  |");
    }

    /**
     * Sets the alliance army size to small
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void setArmyAllianceSmall() {
        ArmySelector.getInstance().setAllianceArmy(DataStorage.generateArmyFromFile("src/main/resources/AllianceSmallArmy.csv"));
        System.out.println("Current Alliance Army: " + ArmySelector.getInstance().getAllianceArmy().toString());
        setChosenArmySizeInGUIAlliance();
    }

    /**
     * Sets the alliance army size to medium
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void setArmyAllianceMedium() {
        ArmySelector.getInstance().setAllianceArmy(DataStorage.generateArmyFromFile("src/main/resources/AllianceMediumArmy.csv"));
        System.out.println("Current Alliance Army: " + ArmySelector.getInstance().getAllianceArmy().toString());
        setChosenArmySizeInGUIAlliance();
    }

    /**
     * Sets the alliance army size to large
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void setArmyAllianceLarge() {
        ArmySelector.getInstance().setAllianceArmy(DataStorage.generateArmyFromFile("src/main/resources/AllianceLargeArmy.csv"));
        System.out.println("Current Alliance Army: " + ArmySelector.getInstance().getAllianceArmy());
        setChosenArmySizeInGUIAlliance();
    }

    /**
     * Sets the horde army size to small
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void setArmyHordeSmall() {
        ArmySelector.getInstance().setHordeArmy(DataStorage.generateArmyFromFile("src/main/resources/HordeSmallArmy.csv"));
        System.out.println("Current Horde Army: " + ArmySelector.getInstance().getHordeArmy());
        setChosenArmySizeInGUIHorde();
    }

    /**
     * Sets the horde army size to medium
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void setArmyHordeMedium() {
        ArmySelector.getInstance().setHordeArmy(DataStorage.generateArmyFromFile("src/main/resources/HordeMediumArmy.csv"));
        System.out.println("Current Horde Army: " + ArmySelector.getInstance().getHordeArmy());
        setChosenArmySizeInGUIHorde();
    }

    /**
     * Sets the horde army size to large
     */

    // Print used while developing to see if it updated. Left in for clarity
    @FXML
    public void setArmyHordeLarge() {
        ArmySelector.getInstance().setHordeArmy(DataStorage.generateArmyFromFile("src/main/resources/HordeLargeArmy.csv"));
        System.out.println("Current Horde Army: " + ArmySelector.getInstance().getHordeArmy());
        setChosenArmySizeInGUIHorde();
    }

    /**
     * Displays the value of the currently selected alliance army size in the GUI
     */
    private void setChosenArmySizeInGUIAlliance() {
        allianceInfantryAmount.setText(String.valueOf(ArmySelector.getInstance().getAllianceArmy().getInfantryUnits().size()));
        allianceRangedAmount.setText(String.valueOf(ArmySelector.getInstance().getAllianceArmy().getRangedUnits().size()));
        allianceCavalryAmount.setText(String.valueOf(ArmySelector.getInstance().getAllianceArmy().getCavalryUnits().size()));
        allianceCommanderName.setText(ArmySelector.getInstance().getAllianceArmy().getCommanderUnits().get(0).getName());
    }

    /**
     * Displays the value of the currently selected horde army size in the GUI
     */
    private void setChosenArmySizeInGUIHorde() {
        hordeInfantryAmount.setText(String.valueOf(ArmySelector.getInstance().getHordeArmy().getInfantryUnits().size()));
        hordeRangedAmount.setText(String.valueOf(ArmySelector.getInstance().getHordeArmy().getRangedUnits().size()));
        hordeCavalryAmount.setText(String.valueOf(ArmySelector.getInstance().getHordeArmy().getCavalryUnits().size()));
        hordeCommanderName.setText(ArmySelector.getInstance().getHordeArmy().getCommanderUnits().get(0).getName());
    }

    /**
     * Creates an instance of Battle and inserts the two currently selected army sizes
     * It then runs the simulate-method
     */
    private void battle() {
        Battle battle = new Battle(ArmySelector.getInstance().getAllianceArmy(), ArmySelector.getInstance().getHordeArmy());
        battle.simulate();
    }


}
