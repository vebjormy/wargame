package ntnu.idatt2001.wargame.application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ntnu.idatt2001.wargame.ArmySelector;
import ntnu.idatt2001.wargame.terrain.CurrentTerrain;
import ntnu.idatt2001.wargame.terrain.Terrain;

import java.io.IOException;
import java.util.Objects;

public class AllianceUnitInformationController {

    private Stage stage;
    private Scene scene;
    private Parent root;

    /**
     * Method for switching to Battle Selection screen
     * Also resets armies to small and terrain to Forest
     *
     * @param event button pressed
     * @throws IOException
     */
    @FXML
    public void switchToBattleSelection(ActionEvent event) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("battle-selection.fxml")));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        ArmySelector.getInstance().resetArmies();
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.FOREST);
    }

    @FXML
    private Text commanderName;

    @FXML
    private void setCurrentCommanderSmall() {
        commanderName.setText("Muradin Bronzebeard");
    }

    @FXML
    private void setCurrentCommanderMedium() {
        commanderName.setText("Malfurion Stormrage");
    }

    @FXML
    private void setCurrentCommanderLarge() {
        commanderName.setText("Arthas Menethil");
    }


}
