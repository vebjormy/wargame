package ntnu.idatt2001.wargame.application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ntnu.idatt2001.wargame.data.ArmyMaker;

import java.io.FileNotFoundException;
import java.util.Objects;


public class GUI extends Application {

    /**
     * Main method for the application
     * Creates all army sizes if they are not already created
     */
    public static void main(String[] args) {
        ArmyMaker armyMaker = new ArmyMaker();
        try {
            armyMaker.allianceSmallArmy();
            armyMaker.allianceMediumArmy();
            armyMaker.allianceLargeArmy();
            armyMaker.hordeSmallArmy();
            armyMaker.hordeMediumArmy();
            armyMaker.hordeLargeArmy();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("start-screen.fxml")));
        Scene scene = new Scene(root);
        stage.setTitle("War Game");
        stage.setScene(scene);
        stage.show();
    }
}

