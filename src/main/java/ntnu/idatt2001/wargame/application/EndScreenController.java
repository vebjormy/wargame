package ntnu.idatt2001.wargame.application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import ntnu.idatt2001.wargame.ArmySelector;
import ntnu.idatt2001.wargame.terrain.CurrentTerrain;
import ntnu.idatt2001.wargame.terrain.Terrain;

import java.io.IOException;
import java.util.Objects;

public class EndScreenController {

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private Text winnerText;

    @FXML
    private Text allianceInfantryAlive;

    @FXML
    private Text allianceRangedAlive;

    @FXML
    private Text allianceCavalryAlive;

    @FXML
    private Text allianceCommanderAlive;

    @FXML
    private Text hordeInfantryAlive;

    @FXML
    private Text hordeRangedAlive;

    @FXML
    private Text hordeCavalryAlive;

    @FXML
    private Text hordeCommanderAlive;

    /**
     * Method for switching to Start Screen.
     * Also resets armies to small and terrain to forest.
     *
     * @param event button pressed
     * @throws IOException
     */
    @FXML
    public void switchToStartScreen(ActionEvent event) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("start-screen.fxml")));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        ArmySelector.getInstance().resetArmies();
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.FOREST); //Resets the BattleSelector to Forest
    }

    /**
     * Method for switching to Battle Selection
     * Also resets armies to small and terrain to forest
     *
     * @param event button pressed
     * @throws IOException
     */
    @FXML
    public void switchToBattleSelection(ActionEvent event) throws IOException {
        root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("battle-selection.fxml")));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        ArmySelector.getInstance().resetArmies();
        CurrentTerrain.getInstance().setCurrentTerrain(Terrain.FOREST); //Resets the BattleSelector to Forest
    }

    /**
     * Method for displaying the alliance units alive after battle
     */
    @FXML
    public void setAliveArmySizeInGUIAlliance() {

        if (ArmySelector.getInstance().getPostBattleAllianceArmy().hasUnits()) {

            if (!(ArmySelector.getInstance().getPostBattleAllianceArmy().getInfantryUnits() == null)) {
                allianceInfantryAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleAllianceArmy().getInfantryUnits().size()));
            }
            if (!(ArmySelector.getInstance().getPostBattleAllianceArmy().getRangedUnits() == null)) {
                allianceRangedAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleAllianceArmy().getRangedUnits().size()));
            }
            if (!(ArmySelector.getInstance().getPostBattleAllianceArmy().getCavalryUnits() == null)) {
                allianceCavalryAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleAllianceArmy().getCavalryUnits().size()));
            }
            if (!(ArmySelector.getInstance().getPostBattleAllianceArmy().getCommanderUnits() == null)) {
                allianceCommanderAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleAllianceArmy().getCommanderUnits().size()));
            }
        }
    }

    /**
     * Method for displaying the horde units alive after battle
     */
    @FXML
    public void setAliveArmySizeInGUIHorde() {

        if (ArmySelector.getInstance().getPostBattleHordeArmy().hasUnits()) {

            if (!(ArmySelector.getInstance().getPostBattleHordeArmy().getInfantryUnits() == null)) {
                hordeInfantryAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleHordeArmy().getInfantryUnits().size()));
            }
            if (!(ArmySelector.getInstance().getPostBattleHordeArmy().getRangedUnits() == null)) {
                hordeRangedAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleHordeArmy().getRangedUnits().size()));
            }
            if (!(ArmySelector.getInstance().getPostBattleHordeArmy().getCavalryUnits() == null)) {
                hordeCavalryAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleHordeArmy().getCavalryUnits().size()));
            }
            if (!(ArmySelector.getInstance().getPostBattleHordeArmy().getCommanderUnits() == null)) {
                hordeCommanderAlive.setText(String.valueOf(ArmySelector.getInstance().getPostBattleHordeArmy().getCommanderUnits().size()));
            }
        }
    }

    /**
     * Method for displaying who won the battle
     */
    @FXML
    public void setWinnerText() {
        if (ArmySelector.getInstance().getPostBattleAllianceArmy().hasUnits()) {
            winnerText.setText("Alliance is victorious!");
        } else if (ArmySelector.getInstance().getPostBattleHordeArmy().hasUnits()) {
            winnerText.setText("Horde is victorious!");
        }
    }
}
