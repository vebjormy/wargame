package ntnu.idatt2001.wargame;


import ntnu.idatt2001.wargame.data.DataStorage;
import ntnu.idatt2001.wargame.units.*;

import java.util.ArrayList;

public class Battle {


    public Battle(Army army1, Army army2) {
        this.army1 = army1;
        this.army2 = army2;
    }

    private final Army army1;
    private final Army army2;

    /**
     * Simulation of a battle between two armies
     * 50/50 roll on who starts, then attacks back and forth until one army is dead
     *
     */
    public void simulate() {
        ArrayList<Unit> army1PostBattleList = new ArrayList<>();    // army1 = alliance
        ArrayList<Unit> army2PostBattleList = new ArrayList<>();    // army2 = horde

        double randomFiftyFifty = Math.random();   // gives a random value between 0 and 1
        boolean firstAttackRoll = randomFiftyFifty < 0.5;  // 50/50 roll on who attacks first

        while (army1.hasUnits() && army2.hasUnits()) {
            if (firstAttackRoll) {
                army1.getRandom().attack(army2.getRandom());
                army2.getAllUnits().removeIf(unit -> unit.getHealth() == 0);
                if (!army2.hasUnits()) {    // checks to see if army2 still has units before it tries to attack
                    break;
                }
                army2.getRandom().attack(army1.getRandom());
                army1.getAllUnits().removeIf(unit -> unit.getHealth() == 0);
            } else if (!firstAttackRoll) {
                army2.getRandom().attack(army1.getRandom());
                army1.getAllUnits().removeIf(unit -> unit.getHealth() == 0);
                if (!army1.hasUnits()) {    // checks to see if army1 still has units before it tries to attack
                    break;
                }
                army1.getRandom().attack(army2.getRandom());
                army2.getAllUnits().removeIf(unit -> unit.getHealth() == 0);
            }
        }

        army1PostBattleList.addAll(army1.getAllUnits());
        army2PostBattleList.addAll(army2.getAllUnits());

        Army army1PostBattle = new Army("PostBattleAlliance", army1PostBattleList);
        Army army2PostBattle = new Army("PostBattleHorde", army2PostBattleList);

        DataStorage.saveArmyToFile(army1PostBattle);
        DataStorage.saveArmyToFile(army2PostBattle);

        if (army1PostBattleList.isEmpty()) {
            System.out.println("Horde Won!");
            System.out.println(army1PostBattle);
            System.out.println(army2PostBattle);
        } else if (army2PostBattleList.isEmpty()) {
            System.out.println("Alliance Won!");
            System.out.println(army1PostBattle);
            System.out.println(army2PostBattle);
        }
    }
}


