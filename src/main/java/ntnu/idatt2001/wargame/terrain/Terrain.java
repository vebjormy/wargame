package ntnu.idatt2001.wargame.terrain;

public enum Terrain {
    HILL,
    PLAINS,
    FOREST,
}
