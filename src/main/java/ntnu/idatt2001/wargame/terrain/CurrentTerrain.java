package ntnu.idatt2001.wargame.terrain;

/**
 * Singleton design pattern to access terrain
 */
public class CurrentTerrain {

    private static final CurrentTerrain currentTerrainInstance = new CurrentTerrain();

    /**
     * Default terrain is FOREST
     */
    private Terrain currentTerrain = Terrain.FOREST;

    /**
     * Method to get current terrain
     *
     * @return current terrain
     */
    public Terrain getCurrentTerrain() {
        return currentTerrain;
    }

    /**
     * Method to set current terrain
     *
     * @param terrain set terrain to Forest, Hill og Plains
     */
    public void setCurrentTerrain(Terrain terrain) {
        this.currentTerrain = terrain;
    }

    /**
     * Method for getting the instance
     *
     * @return the instance
     */
    public static CurrentTerrain getInstance() {
        return currentTerrainInstance;
    }
}
