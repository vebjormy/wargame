package ntnu.idatt2001.wargame.factory;

public enum UnitType {
    INFANTRY,
    RANGED,
    CAVALRY,
    COMMANDER
}
