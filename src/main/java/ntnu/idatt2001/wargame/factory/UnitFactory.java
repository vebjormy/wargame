package ntnu.idatt2001.wargame.factory;

import ntnu.idatt2001.wargame.units.*;

import java.util.ArrayList;
import java.util.List;

public class UnitFactory {

    /**
     * Method that creates and returns a specified unit type with the specified name and health
     *
     * @param unitType one of the four unit types
     * @param name name of the created unit
     * @param health health of the created unit
     * @return
     */
    public static Unit getUnit(UnitType unitType, String name, int health) {

        switch (unitType) {
            case INFANTRY -> {
                return new InfantryUnit(name, health);
            }

            case RANGED -> {
                return new RangedUnit(name, health);
            }

            case CAVALRY -> {
                return new CavalryUnit(name, health);
            }

            case COMMANDER -> {
                return new CommanderUnit(name, health);
            }

            default -> throw new IllegalArgumentException("UnitType not found");
            }
    }

    /**
     * Method to create multiple identical units
     *
     * @param unitType one of the four unit types
     * @param name name of the created units
     * @param health health of the created units
     * @param numberOfUnits number of units to make
     * @return an ArrayList of the units made
     */
    public List<Unit> getMultipleUnits(UnitType unitType, String name, int health, int numberOfUnits) {
        ArrayList<Unit> listOfUnits = new ArrayList<>();

        for (int i = 0; i < numberOfUnits; i++) {
            listOfUnits.add(getUnit(unitType, name, health));
        }
        return listOfUnits;
    }
}
