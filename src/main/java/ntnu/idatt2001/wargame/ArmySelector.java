package ntnu.idatt2001.wargame;

import ntnu.idatt2001.wargame.data.DataStorage;


public class ArmySelector {

    private Army allianceArmy;
    private Army hordeArmy;

    /**
     * Singleton design pattern
     */
    private static final ArmySelector armySelector = new ArmySelector();

    /**
     * Constructor
     *
     * Default army size is small
     */
    public ArmySelector() {
       this.allianceArmy = DataStorage.generateArmyFromFile("src/main/resources/AllianceSmallArmy.csv");
       this.hordeArmy = DataStorage.generateArmyFromFile("src/main/resources/HordeSmallArmy.csv");
    }

    /**
     * Method for getting the instance
     *
     * @return
     */
    public static ArmySelector getInstance() {
        return armySelector;
    }

    /**
     * Method to set alliance army
     *
     * @param army army to set
     */
    public void setAllianceArmy(Army army) {
        this.allianceArmy = army;
    }

    /**
     * Method to get current alliance army
     *
     * @return current alliance army
     */
    public Army getAllianceArmy() {
        return allianceArmy;
    }

    /**
     * Method to get the alliance army after a battle
     *
     * @return alliance army after battle
     */
    public Army getPostBattleAllianceArmy() {
        return DataStorage.generateArmyFromFile("src/main/resources/PostBattleAllianceArmy.csv");
    }

    /**
     * Method to set horde army
     *
     * @param army army to set
     */
    public void setHordeArmy(Army army) {
        this.hordeArmy = army;
    }

    /**
     * Method to get current horde army
     *
     * @return current horde army
     */
    public Army getHordeArmy() {
        return hordeArmy;
    }

    /**
     * Method to get the horde army after a battle
     *
     * @return horde army after battle
     */
    public Army getPostBattleHordeArmy() {
        return DataStorage.generateArmyFromFile("src/main/resources/PostBattleHordeArmy.csv");
    }

    /**
     * Method to reset armies to default state (small)
     */
    public void resetArmies() {
        setAllianceArmy(DataStorage.generateArmyFromFile("src/main/resources/AllianceSmallArmy.csv"));
        setHordeArmy(DataStorage.generateArmyFromFile("src/main/resources/HordeSmallArmy.csv"));
    }
}



