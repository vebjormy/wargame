package ntnu.idatt2001.wargame.data;

import ntnu.idatt2001.wargame.Army;
import ntnu.idatt2001.wargame.units.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class DataStorage {

    /**
     * Method to save to file
     *
     * @param army takes in an army and saves it as a csv-file
     */
    public static void saveArmyToFile(Army army) {
        StringBuilder data = new StringBuilder(army.getName() + "\n");
        for (Unit unit : army.getAllUnits()) {
            data.append(unit.getCsvFormatUnit() + "\n");
        }
        DataHandling.writeToFile(new File("src/main/resources/" + army.getName() + "Army.csv"), String.valueOf(data));
    }

    /**
     * Method for generating an Army from file
     * Splits the lines in the csv files and adds a Unit of the given type to an ArrayList
     * It then puts the Units in the ArrayList in an Army
     *
     * @param filePath the path of the file to load from
     */
    public static Army generateArmyFromFile(String filePath) {
        File file = new File(filePath);
        ArrayList<Unit> unitArrayList = new ArrayList<>();
        ArrayList<String> data = null;
        try {
            data = DataHandling.readFromFile(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (int i = 1; i < data.size(); i++) {
            String[] units = data.get(i).split(",");

            if (units[0].equalsIgnoreCase("InfantryUnit")) {
                unitArrayList.add(new InfantryUnit(units[1], Integer.parseInt(units[2])));
            }
            else if (units[0].equalsIgnoreCase("RangedUnit")) {
                unitArrayList.add(new RangedUnit(units[1], Integer.parseInt(units[2])));
            }
            else if (units[0].equalsIgnoreCase("CavalryUnit")) {
                unitArrayList.add(new CavalryUnit(units[1], Integer.parseInt(units[2])));
            }
            else if (units[0].equalsIgnoreCase("CommanderUnit")) {
                unitArrayList.add(new CommanderUnit(units[1], Integer.parseInt(units[2])));
            }
        }
        Army army = new Army(data.get(0), unitArrayList);
        return army;
    }

}
