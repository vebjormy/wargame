package ntnu.idatt2001.wargame.data;

import ntnu.idatt2001.wargame.Army;
import ntnu.idatt2001.wargame.factory.UnitFactory;
import ntnu.idatt2001.wargame.factory.UnitType;

import java.io.FileNotFoundException;

public class ArmyMaker {

    /**
     * Method for creating alliance small army and saving to file
     *
     * @throws FileNotFoundException
     */
    public void allianceSmallArmy() throws FileNotFoundException {
        UnitFactory unitFactory = new UnitFactory();

        Army allianceSmall = new Army("AllianceSmall");
        allianceSmall.addAll(unitFactory.getMultipleUnits(UnitType.INFANTRY, "Footman", 100, 8));
        allianceSmall.addAll(unitFactory.getMultipleUnits(UnitType.RANGED, "Archer", 80, 5));
        allianceSmall.addAll(unitFactory.getMultipleUnits(UnitType.CAVALRY, "Knight", 100, 3));
        allianceSmall.addAll(unitFactory.getMultipleUnits(UnitType.COMMANDER, "Muradin Bronzebeard", 180, 1));

        DataStorage.saveArmyToFile(allianceSmall);
    }

    /**
     * Method for creating horde small army and saving to file
     *
     * @throws FileNotFoundException
     */
    public void hordeSmallArmy() throws FileNotFoundException {
        UnitFactory unitFactory = new UnitFactory();

        Army hordeSmall = new Army("HordeSmall");
        hordeSmall.addAll(unitFactory.getMultipleUnits(UnitType.INFANTRY, "Grunt", 100, 8));
        hordeSmall.addAll(unitFactory.getMultipleUnits(UnitType.RANGED, "Spearman", 80, 5));
        hordeSmall.addAll(unitFactory.getMultipleUnits(UnitType.CAVALRY, "Raider", 100, 3));
        hordeSmall.addAll(unitFactory.getMultipleUnits(UnitType.COMMANDER, "Gul'dan", 180, 1));

        DataStorage.saveArmyToFile(hordeSmall);
    }

    /**
     * Method for creating alliance medium army and saving to file
     *
     * @throws FileNotFoundException
     */
    public void allianceMediumArmy() throws FileNotFoundException {
        UnitFactory unitFactory = new UnitFactory();

        Army allianceMedium = new Army("AllianceMedium");
        allianceMedium.addAll(unitFactory.getMultipleUnits(UnitType.INFANTRY, "Footman", 100, 30));
        allianceMedium.addAll(unitFactory.getMultipleUnits(UnitType.RANGED, "Archer", 80, 20));
        allianceMedium.addAll(unitFactory.getMultipleUnits(UnitType.CAVALRY, "Knight", 100, 12));
        allianceMedium.addAll(unitFactory.getMultipleUnits(UnitType.COMMANDER, "Malfurion Stormrage", 180, 1));

        DataStorage.saveArmyToFile(allianceMedium);
    }

    /**
     * Method for creating horde medium army and saving to file
     *
     * @throws FileNotFoundException
     */
    public void hordeMediumArmy() throws FileNotFoundException {
        UnitFactory unitFactory = new UnitFactory();

        Army hordeMedium = new Army("HordeMedium");
        hordeMedium.addAll(unitFactory.getMultipleUnits(UnitType.INFANTRY, "Grunt", 100, 30));
        hordeMedium.addAll(unitFactory.getMultipleUnits(UnitType.RANGED, "Spearman", 80, 20));
        hordeMedium.addAll(unitFactory.getMultipleUnits(UnitType.CAVALRY, "Raider", 100, 12));
        hordeMedium.addAll(unitFactory.getMultipleUnits(UnitType.COMMANDER, "Thrall", 180, 1));

        DataStorage.saveArmyToFile(hordeMedium);
    }

    /**
     * Method for creating alliance large army and saving to file
     *
     * @throws FileNotFoundException
     */
    public void allianceLargeArmy() throws FileNotFoundException {
        UnitFactory unitFactory = new UnitFactory();

        Army allianceLarge = new Army("AllianceLarge");
        allianceLarge.addAll(unitFactory.getMultipleUnits(UnitType.INFANTRY, "Footman", 100, 150));
        allianceLarge.addAll(unitFactory.getMultipleUnits(UnitType.RANGED, "Archer", 80, 100));
        allianceLarge.addAll(unitFactory.getMultipleUnits(UnitType.CAVALRY, "Knight", 100, 75));
        allianceLarge.addAll(unitFactory.getMultipleUnits(UnitType.COMMANDER, "Arthas Menethil", 180, 1));

        DataStorage.saveArmyToFile(allianceLarge);
    }

    /**
     * Method for creating horde large army and saving to file
     *
     * @throws FileNotFoundException
     */
    public void hordeLargeArmy() throws FileNotFoundException {
        UnitFactory unitFactory = new UnitFactory();

        Army hordeLarge = new Army("HordeLarge");
        hordeLarge.addAll(unitFactory.getMultipleUnits(UnitType.INFANTRY, "Grunt", 100, 150));
        hordeLarge.addAll(unitFactory.getMultipleUnits(UnitType.RANGED, "Spearman", 80, 100));
        hordeLarge.addAll(unitFactory.getMultipleUnits(UnitType.CAVALRY, "Raider", 100, 75));
        hordeLarge.addAll(unitFactory.getMultipleUnits(UnitType.COMMANDER, "Sylvanas Windrunner", 180, 1));

        DataStorage.saveArmyToFile(hordeLarge);
    }

}
