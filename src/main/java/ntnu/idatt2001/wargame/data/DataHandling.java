package ntnu.idatt2001.wargame.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class DataHandling {


    /**
     * Method for writing to file
     *
     * @param file file to write to
     * @param data data to be written
     */
    public static boolean writeToFile(File file, String data){
        // Would prefer using exceptions instead of returning success for testing but ran out of time
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(data);
            return true;        // for testing
        } catch (IOException e) {
            e.printStackTrace();
            return false;       //for testing
        }
    }

    /**
     * Method for reading from file
     * Try-with-resource
     *
     * @param file file to read from
     */
    public static ArrayList<String> readFromFile(File file) throws FileNotFoundException {
        ArrayList<String> fileList = new ArrayList<>();
        try (Scanner scan = new Scanner(file)) {
            while (scan.hasNext()) {
                String line = scan.nextLine();
                fileList.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileList;
    }
}
