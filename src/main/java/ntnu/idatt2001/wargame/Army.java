package ntnu.idatt2001.wargame;

import ntnu.idatt2001.wargame.units.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

public class Army {

    private final String name;
    private final List<Unit> units;


    /**
     * Simplified constructor for the Army-class
     *
     * @param name name of the Army
     */
    public Army(String name) {
        this.units = new ArrayList<>();
        this.name = name;
    }


    /**
     * Constructor for the Army-class
     *
     * @param name  name of the Army
     * @param units list of units
     */
    public Army(String name, List<Unit> units) {
        this.name = name;
        this.units = units;
    }


    /**
     * Method for getting the name of the Army
     *
     * @return name name of the army
     */
    public String getName() {
        return name;
    }


    /**
     * Method for adding a Unit to a list/army
     *
     * @param unit an instance of a Unit
     * @return true if the unit gets added, false if the unit does not get added
     */
    public boolean add(Unit unit) {
        return this.units.add(unit);
    }


    /**
     * Method for adding a list of Units to an Army
     *
     * @param units a list of Units
     */
    public void addAll(List<Unit> units) {
        this.units.addAll(units);
    }


    /**
     * Method for removing a Unit from a list/army
     *
     * @param unit a Unit
     * @return true if the unit is removed, false if the unit does not get removed
     */
    public boolean remove(Unit unit) {
        return this.units.remove(unit);
    }


    /**
     * Method for checking if a list/army has any units
     *
     * @return true if it has units, false if it is empty
     */
    public boolean hasUnits() {
        return !this.units.isEmpty();
    }


    /**
     * Method for getting all units in a list/army
     *
     * @return the list/list in army
     */
    public List<Unit> getAllUnits() {
        return this.units;
    }


    /**
     * Method for getting a random Unit from the list between 0 and the current size of the list/army
     *
     * @return a random Unit in the list corresponding to the random number generated
     */
    public Unit getRandom() {

        Random r = new Random();
        int random = Math.abs(r.nextInt(this.units.size()));

        return this.units.get(random);
    }


    /**
     * Method for getting a list of all infantry units in an army.
     * Using the armor-stat as a way to identify, every unit type has a unique armor value
     *
     * @return a list of Infantry Units
     */
    public List<Unit> getInfantryUnits() {
        return units.stream().filter(infantryUnits -> infantryUnits.getClass() == InfantryUnit.class).collect(Collectors.toList());

    }


    /**
     * Method for getting a list of all cavalry units in an army.
     * Using the armor-stat as a way to identify, every unit type has a unique armor value
     *
     * @return a list of Ranged Units
     */
    public List<Unit> getRangedUnits() {
        return units.stream().filter(rangedUnits -> rangedUnits.getClass() == RangedUnit.class).collect(Collectors.toList());
    }


    /**
     * Method for getting a list of all cavalry units in an army.
     * Using the armor-stat as a way to identify, every unit type has a unique armor value
     *
     * @return a list of Cavalry Units
     */
    public List<Unit> getCavalryUnits() {
        return units.stream().filter(cavalryUnits -> cavalryUnits.getClass() == CavalryUnit.class).collect(Collectors.toList());
    }


    /**
     * Method for getting a list of all cavalry units in an army.
     * Using the armor-stat as a way to identify, every unit type has a unique armor value
     *
     * @return a list of Cavalry Units
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(commanderUnits -> commanderUnits.getClass() == CommanderUnit.class).collect(Collectors.toList());
    }


    /**
     * toString-method to be able to print Army
     *
     * @return Army with values (name, list of units
     */
    @Override
    public String toString() {
        return  "Name: " + name + ", Units: " + units;
    }

    /**
     * Method to check if two armies are equal
     *
     * @param o the object to compare to
     * @return true if equal, false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return name.equals(army.name) && units.equals(army.units);
    }

    /**
     * Method for generating(?) a unique hashCode to an army
     *
     * @return the unique hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
