module ntnu.idatt2001.wargame {
    requires javafx.controls;
    requires javafx.fxml;


    opens ntnu.idatt2001.wargame to javafx.fxml;
    exports ntnu.idatt2001.wargame;
    exports ntnu.idatt2001.wargame.application;
    opens ntnu.idatt2001.wargame.application to javafx.fxml;
    exports ntnu.idatt2001.wargame.units;
    opens ntnu.idatt2001.wargame.units to javafx.fxml;
    exports ntnu.idatt2001.wargame.terrain;
    opens ntnu.idatt2001.wargame.terrain to javafx.fxml;
}